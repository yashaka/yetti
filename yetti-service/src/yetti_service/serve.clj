(ns yetti-service.serve
  (:require [compojure.route :as route]
            [clojure.java.io :as io])
  (:use compojure.core
        compojure.handler
        carica.core))

(defroutes compojure-handler
  (route/files "/" {:root (config :external-resources)})
  (route/not-found "Not found!"))

(def handler
  (-> compojure-handler
      site))
