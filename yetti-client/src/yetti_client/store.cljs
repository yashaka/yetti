(ns yetti_client.store
  (:require [cljs.reader :as r]
            [datascript :as ds]))

(defn store
  "Save the given application state in HTML5 localstorage"
  [conn]
  (.log js/console (str "Storing..."))
  :implement-me)

(defn load
  "Load the application state from HTML5 localstorage, nil if it doesn't exist"
  []
  :implement-me
  nil)

(defn init-persistence
  "Given an application, watch it for changes and persist via localstorage"
  [app]
  (ds/listen! (:state app) (fn [report]
                             (store (:db-after report)))))
