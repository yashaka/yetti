(ns yetti_client.data)

(defn start-new-list
  "Given an application state, start? (or stop) creating new list as specified"
  [state start?]
  (assoc-in state [:start-new-list?] start?))

(defn start-new-item
  "Given an application state, start? (or stop) creating new list item as specified"
  [state start?]
  (assoc-in state [:start-new-item?] start?))

(defn create-new-list
  "Given an application state, create new list with specified title"
  [state title]
  (-> state
      (update-in [:lists] conj {:title title :items []})
      (assoc-in [:start-new-list?] false)))

(defn create-new-item
  "Given an application state, create new list item with specified name"
  [state name]
  state)

(defn fresh
  "Returns fresh empty application state"
  []
  {:lists [{:title "todo" :items []}
           {:title "doing" :items []}]
   :start-new-list? false
   :start-new-item? false})
