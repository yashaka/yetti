(ns yetti_client.render
  (:require [cljs.core.async :as a]
            [quiescent :as q :include-macros true]
            [quiescent.dom :as d]
            [yetti_client.db :as db])
  (:require-macros [cljs.core.async.macros :as am]
                    [yetti_client.macros :as ym]))
;;   (:use-macros [yetti_client.macros :only [with-input-to-clear]]))


;; from.quiescent.todomvc
(defn enter-key?
  "Return true if an event was the enter key"
  [e]
  (= 13 (.-keyCode e)))


;; from.quiescent.todomvc
(defn class-name
  "Convenience function for creating class names from sets. Nils will
  not be included."
  [classes]
  (apply str (interpose " " (map identity classes))))

(q/defcomponent Adder
  "New list trigger/input"
  [triggered? & {:keys [caption onClick onBlur onKeyDown]}]
  (d/div {:className (class-name #{(when triggered? "editing")})
          :onClick onClick}
         (d/label {:className "add-new-list btn btn-default"}
                  caption)
         (-> (d/input {:className "edit"
                       :defaultValue ""
                       :onBlur onBlur
                       :onKeyDown onKeyDown})
             (q/on-render (fn [node]
                            (when triggered? (.focus node)))))))

;; TODO: refactor to use ym/with-input-to-clear macro
(q/defcomponent AddAList
  "New list trigger/input"
  [start-new-list? channels]
  (d/div {:className "col-md-3"}
    (Adder start-new-list?
           :caption "Add a list..."
           :onClick (fn [e] (am/go (a/>! (:start-new-list? channels) true)))
           :onBlur (fn [e] (am/go (a/>! (:start-new-list? channels) false)))
           :onKeyDown (fn [e]
                        (when (enter-key? e)
                          (let [val (.. e -target -value)]
                            (am/go (a/>! (:create-new-list channels) val)))
                          (set! (.. e -target -value) "")))))
  )
;;          :onKeyDown (ym/with-input-to-clear
;;                       (am/go (a/>! (:create-new-list channels) value)))))

(q/defcomponent AddItem
  "New list item trigger/input"
  [start-new-item? list-eid channels]
  (Adder start-new-item?
         :caption "Add a card..."
         :onClick (fn [e] (am/go (a/>! (:start-new-item? channels) [true list-eid])))
         :onBlur (fn [e] (am/go (a/>! (:start-new-item? channels) [false nil])))
         :onKeyDown (fn [e]
                      (when (enter-key? e)
                        (let [val (.. e -target -value)]
                          (am/go (a/>! (:create-new-item channels) [val list-eid])))
                        (set! (.. e -target -value) "")))))

(q/defcomponent Item
  "List Item"
  [[item selected?] channels]
  (.log js/console (str "selected? - " selected?))
  (d/div {:className (class-name #{"item thumbnail"
                                   (when selected? "selected")})
          :onClick (fn [e] (am/go (a/>! (:selected-item channels) (:eid item))))}
         (:summary item)))

(q/defcomponent TList
  [[tlist start-new-item? focused-list-eid selected] channels]
  (d/div {:className "list panel panel-default col-md-3"}
         (d/div (cond-> {:className "panel-heading"}
                        selected (assoc :onClick (fn [e]
                                                   (am/go (a/>! (:move-item channels)
                                                                {:item selected
                                                                 :to (:eid tlist)})))))
                (:title tlist))
         (d/br {})
         (apply d/div {:className "items"}
                (mapv #(Item [%
                              (= (:eid %) selected)]
                             channels) (:items tlist)))
         (d/div {:className "panel-footer"}
                  (AddItem (and start-new-item? (= (:eid tlist) focused-list-eid))
                           (:eid tlist)
                           channels))))
;;                  "Add an item...")))

(q/defcomponent Lists
  [state channels]
  (apply d/div {:className "lists row"}
         (-> (mapv #(TList [%
                            (db/gattr-value state :start-new-item?)
                            (db/gattr-value state :focused-list-eid)
                            (db/gattr-value state :selected-item)]
                           channels)
                   (db/lists state))
             (conj (AddAList (db/gattr-value state :start-new-list?) channels)))))

(q/defcomponent App
  "The root of the application"
  [state channels]
  (d/div {}
         (d/div {:className "navbar navbar-default"}
                (d/div {:className "navbar-header"}
                       (d/a {:className "navbar-brand"
                             :href "#"}
                            "Yetti")))
         (d/div {:className "container"}
                (Lists state channels))))

;; from.quiescent.todomvc
;; Here we use an atom to tell us if we already have a render queued
;; up; if so, requesting another render is a no-op
(let [render-pending? (atom false)]
  ;; TODO: why the fn definition is inside previous let?
  (defn request-render
    "Render the given application state tree."
    [app]
    (when (compare-and-set! render-pending? false true)
      (.requestAnimationFrame js/window
                              (fn []
                                (q/render (App @(:state app) (:channels app))
                                          (.getElementById js/document "app"))
                                (reset! render-pending? false)))
      (.log js/console "re-renderred."))))
