(ns yetti_client.macros)

(defmacro with-input-to-clear
  [& body]
  `(when (enter-key? e)
    (let [~'value (.. e -target -value)]
      ~body)
    (set! (.. e -target -value) "")))
