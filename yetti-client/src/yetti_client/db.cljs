(ns yetti_client.db
  (:require [datascript :as ds]))

;; Queries
;;
;; Query functions receive aconn, i.e. dereffed conn as first argument.
;; Such implementation was chosen because these queries are used in quiescent
;; components, where conn is already dereffed (because otherwise it would be
;; impossible to define was the state changed or not and so make a decision
;; about the need to render the component)

(defn gdatoms-by-attr
  [aconn attr]
  (ds/q '[:find ?e ?attr ?v
          :in $ ?attr
          :where [?e ?attr ?v]]
        aconn
        attr))

(defn gattr-eid
  [aconn attr]
  (ffirst
   (ds/q '[:find ?e
           :in $ ?attr
           :where [?e ?attr]]
         aconn
         attr)))

(defn eid
  [aconn attr value]
  (ffirst
   (ds/q '[:find ?e
           :in $ ?attr ?value
           :where [?e ?attr ?value]]
         aconn
         attr
         value)))

(defn gattr-value
  [aconn attr]
  (ffirst
   (ds/q '[:find ?v
           :in $ ?attr
           :where [?e ?attr ?v]]
         aconn
         attr)))

(defn get
  [aconn eid attr]
  (ffirst
   (ds/q '[:find ?v
           :in $ ?eid ?attr
           :where [?eid ?attr ?v]]
         aconn
         eid, attr)))

(defn items-by-list
  [aconn list-eid]
  (->> (ds/q '[:find ?i-eid ?i-s
               :in $ ?l-eid
               :where [?e :list-eid ?l-eid]
                      [?e :item-eid ?i-eid]
                      [?i-eid :item/summary ?i-s]]
             aconn, list-eid)
       (map #(zipmap [:eid :summary] %))
       (sort-by :eid)))

(defn list-eid-by-item
  [aconn item-eid]
  (->> (ds/q '[:find ?l-eid
               :in $ ?i-eid
               :where [?e :list-eid ?l-eid]
                      [?e :item-eid ?i-eid]]
             aconn, item-eid)
       (ffirst)))

(defn list-to-item-eid
  [aconn attr value]
  (->> (ds/q '[:find ?e
               :in $ ?a ?v
               :where [?e ?a ?v]]
             aconn, attr, value)
       (ffirst)))

(defn list-to-items
  [aconn]
  (->> (ds/q '[:find ?e ?l-eid ?i-eid
               :where [?e :list-eid ?l-eid]
                      [?e :item-eid ?i-eid]]
             aconn)
       (map #(zipmap [:eid :list-eid :item-eid] %))
       (sort-by :eid)))

(defn lists
  [aconn]
  (->> (ds/q '[:find ?e ?title ?order
               :where [?e :title ?title]
                      [?e :order ?order]]
             aconn)
       (map #(zipmap [:eid :title :order] %))
       (map #(assoc % :items (items-by-list aconn (:eid %))))
       (sort-by :order)))

(defn items
  [aconn]
  (->> (ds/q '[:find ?e ?s
               :where [?e :item/summary ?s]]
             aconn)
       (map #(zipmap [:eid :summary] %))
       (sort-by :eid)))

(defn list-names
  [aconn]
  (->> (lists aconn)
       (map :title)))

(defn all
  [aconn]
  {:global [(gdatoms-by-attr aconn :start-new-list?)
            (gdatoms-by-attr aconn :start-new-item?)
            (gdatoms-by-attr aconn :focused-list-eid)
            (gdatoms-by-attr aconn :selected-item)]
   :lists (lists aconn)
   :items (items aconn)
   :list-to-items (list-to-items aconn)})


;; Transactions
;;
;; Receive conn (not dereffed) as a first argument. So in order to use
;; queries inside Transactions don't forget to deref the conn.

(defn transact-attr
  [conn attr value & {:as more-attr-value-pairs}]
  (ds/transact! conn [(-> {:db/id (gattr-eid @conn attr)
                            attr value}
                           (merge more-attr-value-pairs))]))

(defn start-new-list
  "Given an application state, start? (or stop) creating new list as specified"
  [conn start?]
  (transact-attr conn :start-new-list? start?))

(defn create-new-list
  [conn title]
  (ds/transact! conn [{:db/id -1
                       :title title
                       :order (->> (lists @conn) (map :order) (apply max) inc)}])
  (start-new-list conn false))


(defn start-new-item
  [conn [start? list-eid]]
  (transact-attr conn
                 :start-new-item? start?
                 :focused-list-eid list-eid))

(defn transacted-eid [transaction-report]
  (->> transaction-report
       .-tempids
       .-arr
       second))

(defn create-new-item
  [conn [summary list-eid]]
  (let [item-eid (->> (ds/transact! conn [{:db/id -1
                                           :item/summary summary}])
                     transacted-eid)]
    (ds/transact! conn [{:db/id -1
                         :list-eid list-eid
                         :item-eid item-eid}])
    (start-new-item conn [false nil])))


(defn select-item
  [conn item-eid]
  (transact-attr conn :selected-item item-eid))

(defn move-item
  [conn {:keys [item to]}]
  (let [at (list-to-item-eid @conn :item-eid item)]
    (.log js/console (str "found [" item "] item's list-to-item eid - [" at "]"))
    (ds/transact! conn [{:db/id at :list-eid to}]))
  (transact-attr conn :selected-item nil))

;;TODO: refactor to use {:items {:db/cardinality :db.cardinality/many}
(defn fresh
  "Returns fresh application state db"
  []
  (let [conn (ds/create-conn {})]
    (ds/transact! conn
                  [{:db/id -1
                    :start-new-list? false}
                   {:db/id -2
                    :start-new-item? false
                    :focused-list-eid nil}
                   {:db/id -5
                    :selected-item nil}
                   {:db/id -3
                    :title "todo"
                    :order 1}
                   {:db/id -4
                    :title "doing"
                    :order 2}
                   ])
    (ds/transact! conn [{:db/id -1
                         :item/summary "sample todo item"}])
    (ds/transact! conn [{:db/id -1
                         :item/summary "sample doing item"}])
    (->> (ds/transact! conn [{:db/id -1
                              :list-eid (eid @conn :title "todo")
                              :item-eid (eid @conn :item/summary "sample todo item")}
                             {:db/id -2
                              :list-eid (eid @conn :title "doing")
                              :item-eid (eid @conn :item/summary "sample doing item")}])
         (.-tempids)
         (.-arr)
         (second)
         (.log js/console))
    conn))
