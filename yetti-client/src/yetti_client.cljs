(ns yetti_client
  (:require [yetti_client.render :as render]
            [cljs.core.async :as a]
            [yetti_client.data :as data]
            [yetti_client.db :as db]
            [yetti_client.store :as store])
  (:require-macros
            [cljs.core.async.macros :as am]))

(defn load-app
  "Return a map containing the initial application"
  []
  {:state (or (store/load) (db/fresh))
   :channels  {:start-new-list? (a/chan)
               :create-new-list (a/chan)
               :start-new-item? (a/chan)
               :create-new-item (a/chan)
               :selected-item (a/chan)
               :move-item (a/chan)}
   ;; for each channel we have db/<transact-method> with the same name
   ;; in order ot simplify the code and make more DRY...
   ;; It would be reasonable to implement all db/transactions as
   ;; multimethods dispatched by "update key", i.e. :start-new-list?,
   ;; :move-item, etc... This way we will not need :consumers in app state
   ;; TODO: consider and implement
   :consumers {:start-new-list? db/start-new-list
               :create-new-list db/create-new-list
               :start-new-item? db/start-new-item
               :create-new-item db/create-new-item
               :selected-item db/select-item
               :move-item db/move-item}})

;; This will init both "frontend" transactions to state and
;; rendering based on new data come to channels.
;; But there also will be updates from backend.
;; Hence TODO: add support of "backend" transactions
(defn init-updates
  "For every entry in a map of channel identifiers to consumers,
  initiate a channel listener which will update the application state
  using the appropriate function whenever a value is recieved, as
  well as triggering a render."
  [app]
  (doseq [[ch transact-fn] (:consumers app)]
    (am/go (while true
             (let [val (a/<! (get (:channels app) ch))]
               (.log js/console (str "on channel [" ch "], recieved value [" val "]"))

               (transact-fn (:state app) val)
               (.log js/console (str "current state - " (db/all @(:state app))))
               (render/request-render app))))))

(defn ^:export main
  "Application entry point"
  []
  (let [app (load-app)]
    (store/init-persistence app)
    (init-updates app)
    (render/request-render app)))
