(defproject yetti-client "0.1.0"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :plugins [[lein-cljsbuild "1.0.3"]]
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/clojurescript "0.0-2227"]
                 [org.clojure/core.async "0.1.303.0-886421-alpha"]
                 [quiescent/quiescent "0.1.3"]
                 [datascript "0.1.4"]]
  :resource-paths ["resources" "lib"]
  ;; Note: it is not idiomatic to compile to, or serve from, the
  ;; project root. TODO: fix later
  :cljsbuild {:builds
              {:dev {:source-paths ["src"]
                     :compiler
                     {:output-dir "generated"
                      :output-to "generated/main-dev.js"
                      :optimizations :none
                      :source-map true
                      :pretty-print true
                      }}
               ;; cljsbuild prod currently fails with
               ;;   No implementation of method: :make-reader of protocol:
               ;;   #'clojure.java.io/IOFactory found for class: nil
               ;; TODO fix later
               :prod {:source-paths ["src"]
                      :compiler
                      {:output-to "main.js"
                       :optimizations :advanced
                       :preamble ["react-0.10.0.min.js"]
                       :externs ["react-externs-0.10.0.js"]
                       :pretty-print false
                       :closure-warnings {:non-standard-jsdoc :off}}}}})
