This should be a Trello alternative (kanban board with lists and movable todo items in them)

So far only client part implemented (no backend, no storing of any data created)

Available features
- add a new list
- add a new item to the list
- move item from one list to another (by selecting an item and clicking on the destination list header)

Next todos:
- serve app as ring resource and deploy on heroku
- store all changes to app state via web-sockets on backend (in datomic db)
- get updates from backend via web-sockets and transact them to app state

To run app open yetti-client/index-dev.html

To build:
$ cd yetti-client
$ lein cljsbuild once dev

Technology Stack:
Used: Quiescent, core.async, DataScript
Waiting: Ring, Composure, Datomic, web-sockets (http-kit)
